#pragma once
namespace Portable {
class Register {
 public:
  int x;
};
}  // namespace Portable

namespace RTOS {
class Register : public Portable::Register {
 public:
  Register();
};
}  // namespace RTOS

namespace Linux {
class Register : public Portable::Register {
 public:
  Register();
};
}  // namespace Linux
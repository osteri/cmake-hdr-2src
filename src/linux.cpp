#include <register.hpp>

#include <iostream>

namespace Linux {

  Register::Register() { std::cout << "Hello from Linux" << '\n'; }

}  // namespace Linux
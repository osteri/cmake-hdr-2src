# CMake: one header, multiple implementations

The project demonstrates on how to have one header and multiple implementation files `src/*.cpp`, and how to organize CMake project and use different cpp files with different targets. RTOS and Linux libraries are created from a single header file `register.hpp`, and then they are linked with `Register::Linux` or either `Register::RTOS` library. See `example` directory for using the library targets. Separation has the benefit of leaving unused components out of the compilation and linking. So this makes compilation **and** linking faster if required. For example: in RTOS platform, we don't want to include `<iostream>`, but in Linux platform it's helpful for debugging and might have other use cases. Possibilities are endless.

## Build examples

```
cmake -S . -B build && (cd build && make -j)
```

## Run

```
./build/linux-example
./build/rtos-example
```
The RTOS binary size is less (because of different implementation):
```
ls -la build/*-example
-rwxrwxr-x. 1 user user 25960 Oct  3 16:05 build/linux-example
-rwxrwxr-x. 1 user user 25176 Oct  3 16:05 build/rtos-example
```

Cool, now we are able to add new implementations of different platforms without bloating other platform implementations. Just don't go too wild with the implementation differences, otherwise the cons can outweight pros of this approach, and diagnosing compilation errors could become more difficult.
